'use strict';

var SwaggerExpress = require('swagger-express-mw');
var SwaggerUi = require('swagger-tools/middleware/swagger-ui');

var cors = require('cors')
var app = require('express')();
module.exports = app; // for testing

app.use(cors()); // enable cors for all
app.options('*', cors()); // enable cors preflight for all


var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  app.use(SwaggerUi(swaggerExpress.runner.swagger));

  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

});
