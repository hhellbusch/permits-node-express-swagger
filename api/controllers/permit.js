'use strict';

var util = require('util');
var db = require('../../config/db')();

// example eletrical permit application 
// https://www.columbus.gov/WorkArea/DownloadAsset.aspx?id=68349

module.exports = {
  addElectrical: addElectrical,
  addStructural: addStructural,
  getPermit: getPermit,
  deletePermit: deletePermit,
  setStatus:setStatus
};

const PERMIT_TABLE = 'permit';

function addPermit(req, res, permitType) {
  let permit = handlePermitInput(req);

  permit.type = permitType;
  permit.status = "IN_PROGRESS";
  let result = db.save(PERMIT_TABLE, permit);

  // respond
  let responseObj = {
    "success" : result,
    "data" : permit,
    "description": permitType + " permit application submitted"
  };
  
  res.json(responseObj);
}

function handlePermitInput(req) {
  // handle input
  let jobSiteLocation = req.body.jobSiteLocation;
  let propertyOwner = req.body.propertyOwner;
  let permitHolder = req.body.permitHolder;

  let permit = {
    jobSiteLocation: jobSiteLocation,
    propertyOwner: propertyOwner,
    permitHolder: permitHolder,
  };
  return permit;
}

function addElectrical(req, res, next) {
  addPermit(req, res, "electrical");
}

function addStructural(req, res, next) {
  addPermit(req, res, "structural");
}


function getPermit(req, res, next) {
  // handle input
  let id = req.swagger.params.id.value;

  // ask database
  let permit = db.find(PERMIT_TABLE, id);

  // check to make sure we found the correct type of permit
  // based on the address
  // if we were using a "real" db; we could filter by type instead..
  let permitType = getPermitTypeFromApiPath(req);
  if (!permit || permitType != permit.type) {
    return res.status(204).send();
  }

  // respond
  let responseObj = {
    "success": 1,
    "description" : "a permit",
    "data": permit,
  };
  return res.json(responseObj);
}

function deletePermit(req, res, next) {
  let id = req.swagger.params.id.value;
  let permit = db.find(PERMIT_TABLE, id);

  // check to make sure we found the correct type of permit
  // based on the address
  // if we were using a "real" db; we could filter by type instead..
  let permitType = getPermitTypeFromApiPath(req);
  if (!permit || permitType != permit.type) {
    return res.status(204).send();
  }

  let result = db.remove(PERMIT_TABLE, id);
  let responseObj = {
    success: 1,
    description: "permit deleted"
  };
  return res.json(responseObj);
}

function setStatus(req, res, next) {
  let status = req.swagger.params.status.value;
  let id = req.swagger.params.id.value;
  let permit = db.find(PERMIT_TABLE, id);

  let permitType = getPermitTypeFromApiPath(req);
  if (!permit || permitType != permit.type) {
    return res.status(204).send();
  }
  
  let updateRes = db.update(PERMIT_TABLE, id, {status: status});
  let responseObj = {
    success: updateRes,
    description: "permit status updated"
  };
  return res.json(responseObj);
}

function getPermitTypeFromApiPath(req)
{
  let apiPath = req.swagger.apiPath;
  let pathParts = apiPath.split('/');
  let permitType = pathParts[1];
  return permitType;
}