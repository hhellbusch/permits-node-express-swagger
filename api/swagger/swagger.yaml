swagger: "2.0"
info:
  version: "0.0.1"
  title: Hello World App
# during dev, should point to your local machine
# host: localhost:10010
# basePath prefixes all resource paths 
basePath: /
# 
schemes:
  # tip: remove http to make production-grade
  - http
  - https
# format of bodies a client can send (Content-Type)
consumes:
  - application/json
# format of the responses to the client (Accepts)
produces:
  - application/json

paths:
  /electrical:
    x-swagger-router-controller: permit
    post:
      operationId: addElectrical
      description: Create a eletrical permit
      consumes:
        - application/json
      parameters:
        - in: body
          name: electricalPermit
          description: the electrical permit to Create
          schema:
            $ref: "#/definitions/PermitInput"
      responses:
        "200": 
          description: Success
          schema:
            $ref: "#/definitions/PermitResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /electrical/{id}:
    x-swagger-router-controller: permit
    get:
      operationId: getPermit
      description: get details of an electrical permit
      parameters:
        - name: id
          type: string
          in: path
          required: true
      responses:
        "200":
          description: Success
          schema:
            $ref: "#/definitions/PermitResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    delete:
      operationId: deletePermit
      description: retract an electrical permit
      parameters:
        - name: id
          type: string
          in: path
          required: true
      responses:
        "200":
          description: Success
          schema:
            $ref: "#/definitions/GeneralResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /electrical/{id}/status/:
    x-swagger-router-controller: permit
    post:
      operationId: setStatus
      description: update the status of the permit request
      parameters:
        - name: id
          type: string
          in: path
          required: true
        - in: body
          name: status
          description: the new status
          schema:
           type: string
      responses:
        "200":
          description: Success
          schema:
            $ref: '#/definitions/GeneralResponse'

  /structural:
    x-swagger-router-controller: permit
    post:
      operationId: addStructural
      description: Create a structural permit
      consumes:
        - application/json
      parameters:
        - in: body
          name: structuralPermit
          description: the structural permit to Create
          schema:
            $ref: "#/definitions/PermitInput"
      responses:
        "200": 
          description: Success
          schema:
            $ref: "#/definitions/PermitResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /structural/{id}:
    x-swagger-router-controller: permit
    get:
      operationId: getPermit
      description: get details of an structural permit
      parameters:
        - name: id
          type: string
          in: path
          required: true
      responses:
        "200":
          description: Success
          schema:
            $ref: "#/definitions/PermitResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    delete:
      operationId: deletePermit
      description: retract an structural permit
      parameters:
        - name: id
          type: string
          in: path
          required: true
      responses:
        "200":
          description: Success
          schema:
            $ref: "#/definitions/GeneralResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /structural/{id}/status/:
    x-swagger-router-controller: permit
    post:
      operationId: setStatus
      description: update the status of the permit request
      parameters:
        - name: id
          type: string
          in: path
          required: true
        - in: body
          name: status
          description: the new status
          schema:
           type: string
      responses:
        "200":
          description: Success
          schema:
            $ref: '#/definitions/GeneralResponse'

  /swagger:
    x-swagger-pipe: swagger_raw

# complex objects have schema definitions
definitions:
  GeneralResponse:
    type: object
    properties:
      success:
        type: number
        description: returns 1 if successful
      description:
        type: string
        description: a short comment
    required:
      - success

  PermitResponse:
    allOf:
      - $ref: '#/definitions/GeneralResponse'
      - properties:
          data: 
            $ref: '#/definitions/Permit'

  Location:
    type: object
    properties:
      streetAddress:
        type: string
        example: 1234 Rainbow Rd
      # city:
      #   type: string
      #   example: Columbus
      # state:
      #   type: string
      #   example: Ohio
    required: 
      - streetAddress
      # - city
      # - state


  Person:
    type: object
    properties:
      name:
        type: string
        description: name of the person
        example: Hank
      email:
        type: string
        description: email
        example: hank@example.com
      address:
        $ref: "#/definitions/Location"
    required:
      - name
      # - email
      - address

  PermitHolder:
    type: object
    properties:
      registrationID:
        type: string
        description: the registration ID
        example: 1234ASDF
      name:
        type: string
        description: name of the contractor or company
        example: The Stacks Contracting
      telephone:
        type: string
        description: telephone number
        example: 867-5309
      email:
        type: string
        description: email address of project manager
        example: mark@example.com
    required:
      - registrationID
      - name
      - telephone
      # - email

  Permit:
    allOf:
      - $ref: '#/definitions/PermitInput'
      - properties:
          status:
            type: string
            example: IN_PROGRESS
            description: IN_PROGRESS, APPROVED, DENIED
          type:
            type: string
            description: the type of permit
            example: electrical
          id:
            type: string
            description: unique identifier for the permit
            example: 3460da9114df5651f3dbd5ee0c108dada4d65f6f
        required:
          - status
          - type
          - id


  PermitInput:
    type: object
    properties:
      status: 
        type: string
        example: APPROVED
      jobSiteLocation: 
        $ref: "#/definitions/Location"
      propertyOwner: 
        $ref: "#/definitions/Person"
      permitHolder: 
        $ref: "#/definitions/PermitHolder"
    required:
      - jobSiteLocation
      - propertyOwner
      - permitHolder

  ErrorResponse:
    required:
      - message
    properties:
      message:
        type: string
