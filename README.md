# Permit Web-service

This is an example permit web-service. I built this
to interact with a project running on Red Hat's 
Process Automation Manager.

This web-service handles two types of permits -
* Electrical
* Structural


# Installing and Running

First install the dependencies with npm - 

```
npm install
```

You can launch the application by running

```
node app.js
```

If you have the swagger tooling installed - you can use

```
swagger project start


# Sample Output
Starting: /home/hhellbusch/git/hhellbusch/pam-solar/permits/app.js...
  project started here: http://localhost:10011/
  project will restart on changes.
  to restart at any time, enter `rs`
```

This has the added benefit of watching the project and restarting
it when you make a change in a file.  You could also use nodemon for this.


# API Docs

The API docs are generated from Swagger-UI. You can access them by
pointing your browser to `/docs`.



# Technology Choices

This project was scaffolded with swagger-node - 
https://github.com/swagger-api/swagger-node

This has the promise of validating your code and actual results of your
runtime.

As of 2018/07/17 it only support swagger 2.0 and not OpenAPI 3.0,
but according to https://github.com/swagger-api/swagger-node/issues/514
there are plans to add support for 3.0 in the future.


Data is stored in memory!  This is managed by `config/db.js`.
If one desired to plug in a real datastore (e.g. mongo or couch)
it would be simple to change this file accordingly and not have to
change other code.

## Things I don't like about the starter that swagger generates

### Not much for docs

I wasn't able to find much for docs - everything requires digging into the source
code or github tickets...


### Swagger UI (docs) not included by default

Was able to figure out a way to include it after reading
several GitHub pages. The one that finally gave me an answer -
https://github.com/swagger-api/swagger-node/issues/524#issuecomment-338164612

### Not very helpful error messages

When your swagger file has an error, by default, the runtime just says 

```
Error initializing middleware
Swagger document(s) failed validation so the server cannot start
```

With this one, able to work around it by copy / pasting into the
swagger editor - https://editor.swagger.io/



Or if you have an error in your spec vs actual code

```
Error: Response validation failed: failed schema validation
```

Can change this in a few different ways - see https://github.com/apigee-127/swagger-tools/issues/108#issuecomment-121734617

Easiest was to simply run by running the application with

```
DEBUG=swagger-tools* swagger project start 
```

After starting the project this way, you get a lot more details.

As an example. Howevver it still doesn't say exactly what part of the schema is invalid.
Just that something was supposed to be an object, but really was undefined.
```
  swagger-tools:middleware:ui POST /electrical +8s
  swagger-tools:middleware:ui   Will process: no +0ms
  swagger-tools:middleware:metadata POST /electrical +0ms
  swagger-tools:middleware:metadata   Is a Swagger path: true +0ms
  swagger-tools:middleware:metadata   Is a Swagger operation: true +0ms
  swagger-tools:middleware:metadata   Processing Parameters +1ms
  swagger-tools:middleware:metadata     electricalPermit +11ms
  swagger-tools:middleware:metadata       Type: object +0ms
  swagger-tools:middleware:metadata       Value provided: true +0ms
  swagger-tools:middleware:metadata       Value: [object Object] +1ms
  swagger-tools:middleware:security POST /electrical +1ms
  swagger-tools:middleware:security   Will process: yes +0ms
  swagger-tools:middleware:validator POST /electrical +0ms
  swagger-tools:middleware:validator   Will process: yes +0ms
  swagger-tools:middleware:validator   Request validation: +1ms
  swagger-tools:middleware:validator     Validation: succeeded +1ms
  swagger-tools:middleware:router POST /electrical +1ms
  swagger-tools:middleware:router   Will process: yes +0ms
  swagger-tools:middleware:router   Route handler: permit_addElectrical +0ms
  swagger-tools:middleware:router     Missing: no +0ms
  swagger-tools:middleware:router     Ignored: no +0ms
  swagger-tools:middleware:router     Using mock: no +0ms
  swagger-tools:middleware:validator   Response validation: +3ms
  swagger-tools:middleware:validator     Response code: 200 +0ms
  swagger-tools:middleware:validator     Validation: failed +1ms
  swagger-tools:middleware:validator   Reason: Failed schema validation +0ms
  swagger-tools:middleware:validator   Errors: +0ms
  swagger-tools:middleware:validator     0: +0ms
  swagger-tools:middleware:validator       code: INVALID_TYPE +0ms
  swagger-tools:middleware:validator       message: Expected type object but found type undefined +0ms
  swagger-tools:middleware:validator       path: [] +0ms
  swagger-tools:middleware:validator   Stack: +1ms
  swagger-tools:middleware:validator       at throwErrorWithCode (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/swagger-tools/lib/validators.js:121:13) +0ms
  swagger-tools:middleware:validator       at Object.module.exports.validateAgainstSchema (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/swagger-tools/lib/validators.js:176:7) +0ms
  swagger-tools:middleware:validator       at /home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/swagger-tools/middleware/swagger-validator.js:141:22 +0ms
  swagger-tools:middleware:validator       at /home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/async/lib/async.js:356:13 +0ms
  swagger-tools:middleware:validator       at async.forEachOf.async.eachOf (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/async/lib/async.js:233:13) +0ms
  swagger-tools:middleware:validator       at _asyncMap (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/async/lib/async.js:355:9) +0ms
  swagger-tools:middleware:validator       at Object.map (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/async/lib/async.js:337:20) +0ms
  swagger-tools:middleware:validator       at validateValue (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/swagger-tools/middleware/swagger-validator.js:134:11) +0ms
  swagger-tools:middleware:validator       at ServerResponse.res.end (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/swagger-tools/middleware/swagger-validator.js:252:9) +0ms
  swagger-tools:middleware:validator       at _cb (/home/hhellbusch/git/hhellbusch/pam-solar/permits/node_modules/restify/lib/response.js:337:14) +0ms
```

