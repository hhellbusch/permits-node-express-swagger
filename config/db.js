'use strict';

var crypto = require('crypto');

/**
 * Variation of
 * https://github.com/samuxyz/movie-collection/blob/master/config/db.js
 */
module.exports = function(){
  var database = {};

  return {
    
    save: function (table, object) {
      object.id = crypto.randomBytes(20).toString('hex');
      if (database[table] === undefined) {
        database[table] = [];
      }
      database[table].push(object);
      return 1;
    },

    // if id is specified, return just that element; 
    // else return the whole table
    find(table, id) {
      if (database[table] === undefined) {
        return false;
      }

      if (id) {
        return database[table].find(element => {
          return element.id === id;
        });
      } else {
        return database[table];
      }
      return false;
    },

    // pretty inefficient way to find and remove an element from a list...
    // basically builds a new array every time something is removed
    remove: function(table, id) {
      if (database[table] === undefined) {
        return false;
      }
      let found = 0;

      database[table] = database[table].filter(element => {
        if (element.id === id) {
          found = 1;
        } else {
          return element.id !== id;
        }
      });
      return found;
    },

    update: function(table, id, object) {
      if (database[table] === undefined) {
        return 0;
      }

      let index = database[table].findIndex(element => {
        return element.id === id;
      });
      
      if (index !== -1) {
        // copy all indicies into the obj
        Object.entries(object).forEach(([key, value]) => {
          database[table][index][key] = value;
        });
        return 1;
      }
      return 0;
    }

  };
};
